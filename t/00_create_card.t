#!/usr/bin/perl

use strict;
use warnings;

use lib 'blib/lib';
use Test::More qw(no_plan);

#
# Test that we can create a pack of cards
#
BEGIN { use_ok 'Icydee::Card'; };

can_ok('Icydee::Card', 'stringify');

my $card = new Icydee::Card(name => 'King', suit => 'Heart');

is ($card->name, 'King',    "Card is correct");
is ($card->suit, 'Heart',   "Suit is correct");


1;
