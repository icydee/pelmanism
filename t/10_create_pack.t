#!/usr/bin/perl

use strict;
use warnings;

use lib 'blib/lib';
use Test::More qw(no_plan);

#
# Test that we can create a pack of cards
#
BEGIN { use_ok 'Icydee::Pack'; };

can_ok('Icydee::Pack', 'newpack');
can_ok('Icydee::Pack', 'get_card');

#
# Create a new pack, and initialise it
#
my $pack = Icydee::Pack->new;
$pack->newpack;
$pack->shuffle;

# Check that there are 54 cards are in the pack
for my $index (0..53) {
    ok ($pack->get_card($index), "Can get a card");
    print STDERR "Card = [".$pack->get_card($index)->stringify."]\n";
}

1;
