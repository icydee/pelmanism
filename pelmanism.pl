#!/usr/bin/perl

use lib 'lib';
use strict;
use warnings;
use Data::Dumper;

use Icydee::Pack;
use Icydee::Card;

#
# The game of Pelmanism
#

#
# Create a 'pack' object so we can add methods such as 'shuffle'
#
my $pack = new Icydee::Pack->new;
$pack->newpack;
$pack->shuffle;



# the cards are layed out in positions 0 - 53 the computer quesses which locations have pairs of cards
# Store this as a hashref
#
# The value of the hashref,
#   'unknown' - the card has not yet been viewed
#   'viewed'  - the card has been viewed
#   'removed' - the card has been guessed (and removed) correctly.
#
my %layout;
for my $i (0..53) {
    $layout{$i}{status} = 'unknown';           # the card has not yet been viewed
    $layout{$i}{card}   = $pack->get_card($i);      # the value of the card
}

#
# To help, keep a hash of all 'known' cards in the layout
# $known_card{name} gives the card name (for example Ace) and the value is the index in the layout hash
#
my @known_cards = ();

#
# The number of cards removed
#
my $cards_banked = 0;

#
# The number of cards turned over;
#
my $cards_turned_over = 0;

#
# The order that cards were banked
#
my @card_order;

# The index of the drawn cards from the layout
my $idx_1;
my $idx_2;

#
# main loop, quess until all cards have been removed from the board
#
while ($cards_banked < 54) {
    #
    # If the second card drawn on the previous round matches a known card, then we know a pair
    #
    my $card_1;
    my $card_2;

    #
    # See if there are any pairs already in the remembered array
    #
    my $existing_idx;
    my $found_pair = ($idx_1, $idx_2) = matches_pair();
    if ($found_pair == 2) {
        $idx_1 = $idx_1;
        $idx_2 = $idx_2;
        $card_1 = turn_over_card($idx_1);
        $card_2 = turn_over_card($idx_2);
    }
    else {
        # randomly find one of the unknown cards
        $idx_1  = choose_at_random(grep {$layout{$_}{status} eq 'unknown'} keys %layout);
        $card_1 = turn_over_card($idx_1);

        #
        # Here we check if the first card drawn matches one that we already know about
        #
        my $existing_idx = matches_existing($idx_1);
        if ($existing_idx) {
            $idx_2 = $existing_idx;
            $card_2 = turn_over_card($idx_2);
        }
        else {
            # otherwise we draw a second card

            # randomly find one of the other unknown cards
            $idx_2  = choose_at_random(grep {$layout{$_}{status} eq 'unknown'} keys %layout);
            $card_2 = turn_over_card($idx_2);
        }
    }

    # see if we have a match
    if ($card_1->name eq $card_2->name) {
        $cards_banked += 2;
        push @card_order, $card_1;
        push @card_order, $card_2;
        #
        # Remove the cards we matched (if any) from the known_cards array
        #
        forget_card($card_1);
        forget_card($card_2);
    }
    else {
        remember_card($card_1, $idx_1);
        remember_card($card_2, $idx_2);
    }
}

#
# Display the order in which the cards were drawn
#

print "Cards were drawn in the following order\n";
for my $card (@card_order) {
    print $card->stringify."\n";
}
print "A total of $cards_turned_over cards were turned over\n";

#
# Randomly choose an 'unchosen' card
#
sub choose_at_random {
    my @keys = @_;

    my $index = int(rand(@keys));
    return $keys[$index];
}

#
# Turn over a card
#
sub turn_over_card {
    my ($idx) = @_;

    my $card = $layout{$idx}{card};
    $layout{$idx}{status} = 'known';
    $cards_turned_over++;

    return $card;
}


#
# Remove a card from the known card array
#
sub forget_card {
    my ($card) = @_;

    @known_cards = grep {$_->{card}->name ne $card->name or $_->{card}->suit ne $card->suit} @known_cards;
}

#
# Subroutine to remember the location of a card in the known card array
#
sub remember_card {
    my ($card, $idx) = @_;

    # remember the card unless it is already known
    if (not grep {$_->{idx} == $idx} @known_cards) {
        push @known_cards, {
            card    => $card,
            idx     => $idx,
        };
    }
}

#
# See if an index matches an existing known card
#
sub matches_existing {
    my ($idx) = @_;

    return unless $idx;
    my $card = $layout{$idx}{card};
    #
    # see if the card is already known
    #
    my ($matched_card) = grep {$_->{card}->name eq $card->name and $_->{card}->suit ne $card->suit} @known_cards;

    return $matched_card->{idx};
}

#
# See if there are any two cards memorised that are the same
#
sub matches_pair {

    for my $name (qw(Ace two three four five six seven eight nine ten Jack Queen King zero)) {
        my ($known_1, $known_2) = grep {$_->{card}->name eq $name} @known_cards;
        if ($known_2) {
            return ($known_1->{idx}, $known_2->{idx});
        }
    }
    return;
}

1;
