package Icydee::Card;
use Moose;              # automatically turns on strict and warnings

use Data::Dumper;
#
# Routines related to creating and printing a card
#

# A card is one of Ace, 2..10, Jack, Queen, King or Joker

has 'name' => (is => 'ro');
has 'suit' => (is => 'ro');

#
# We should ensure that 'name' and 'suit' are in one of the enumerated values
# when we create a card
#

#
# get the name of the card, e.g. 'Ace of Hearts'
#
sub stringify {
    my $self = shift;

    my $str = $self->name." of ".$self->suit."s";
    return $str;
}

1;
