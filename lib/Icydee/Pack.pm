package Icydee::Pack;
use Moose;              # automatically turns on strict and warnings
use Icydee::Card;
use Data::Dumper;

#
# Routines related to creating and printing a pack of cards
# which consists of 4 suites Ace to King and two Jokers
#

has 'cards' => (is => 'rw', isa => 'ArrayRef');

#
# As a first iteration, create a card as H-1 throuh H-13 (Hearts 1-13) and
# worry about a better representation later
# A joker is J-1 or J-2
#

#
# Create a new pack
#
sub newpack {
    my $self = shift;

    #
    # Create a standard pack
    #
    $self->cards([]);

    for my $suit (qw(Heart Spade Diamond Club)) {
        for my $name (qw(Ace two three four five six seven eight nine ten Jack Queen King)) {
            push(@{$self->cards}, Icydee::Card->new(name => $name, suit => $suit))
        }
    }
    push(@{$self->cards}, Icydee::Card->new(name => 'zero', suit => 'Joker1'));
    push(@{$self->cards}, Icydee::Card->new(name => 'zero', suit => 'Joker2'));
    return $self;
};


#
# Shuffle auxilliary routine to return subroutine
# that returns a pack of cards in a shuffled order.
#
sub _aux_shuffle {
    my $self = shift;
    my @cards = (@{$self->cards});
    return sub {
        if (@cards) {
            my $i = int(rand(scalar(@cards)));
            my $card = $cards[$i];
            splice @cards, $i, 1;
            return $card;
        }
        return;
    };
}

#
# Shuffle the pack into a random order
#
sub shuffle {
    my $self = shift;

    my $aux = $self->_aux_shuffle;
    my @new_cards = ();
    while (my $card = &$aux) {
        push(@new_cards, $card);
    }
    $self->cards([@new_cards]);
}

#
# Return the card at position 'index'
#
sub get_card {
    my ($self, $index) = @_;

    return ${$self->cards}[$index];
}
1;
